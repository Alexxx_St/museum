import React from "react";
import {Link } from "react-router-dom";


const Card = (props) => (
    <div className="card">
        <img src={props.imgUrl} alt={props.alt || "image"}/>
        <div className="card-content">
            <h2>{props.teacherName}</h2>
            <span>{props.lifeTime}</span>
            <p>{props.shortText}</p>
        </div>
    </div>
);
const CardContainer = (props) => (
    <div className="cards-container">
        {props.cards.map((card)=>(
            <Card
                teacherName  = {card.teacherName}
                lifeTime  = {card.lifeTime}
                imgUrl = {card.imgUrl}
                //onClick = {}
            />
        ))
        }
    </div>
);


function Gallery(){

    const cardsData = [
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},
{id: 1, teacherName: 'Иванов Иван Иванович', lifeTime: '1968-1998', imgUrl: 'img/FridmanAD.jpeg'},


    ];


    return(
        <div id="gui__container">
            <Link to="/" className="btn-home">
                На главную
            </Link>
            <CardContainer cards={ cardsData } />
        </div>

    );
}

export default Gallery;
развертка проекта:
=====================
Клонировать репозиторий
-----------------------------------
При необходимости донастройте ваш web-сервер 
чтобы он видел файл index.php

Установить Nodejs и npm
-----------------------------------
npm -i для скачивания модулей
-----------------------------------
Старт разработки проекта:
=====================
запуск сборщика стилей 'npm run gulp' 
-----------------------------------
запуск сборщика js 'npm run start'
-----------------------------------
единоразовый запуск сборщика js 'npm run build'
-----------------------------------
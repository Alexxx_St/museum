import React, { Component } from "react";
import ReactDom from "react-dom";
import Main from "./components/blocks/Main";


ReactDom.render(<Main />, document.getElementById("root"));

import React from "react";
import "fullpage.js/vendors/scrolloverflow";
import {Link } from "react-router-dom";
import ReactFullpage from "@fullpage/react-fullpage";
//import ThirtySection from "./historySections/ThirtySection";
//import Navigation from "../elems/Navigation";

function History(){
    return (
        <div id="gui__container">
            <Link to="/" className="btn-home">
               На главную
            </Link>

            <button className="btnTop" onClick = {() => {fullpage_api.moveTo(1, 0)}} >
                Наверх
            </button>
        <ReactFullpage
          scrollOverflow = {true}
          sectionsColor  = {["orange", "purple", "green"]}
          //onLeave      = {this.onLeave.bind(this)}
          //afterLoad    = {this.afterLoad.bind(this)}
          //onClick        = {this.onClick.bind(this)}

          render         = {({ state, fullpageApi }) => {
              return (
                  <div >

                      <ThirtySection/>
                      <div className="section">
                          <div className="slide">
                              <h3>Slide 2.1</h3>
                          </div>
                          <div className="slide">
                              <h3>Slide 2.2</h3>
                          </div>
                          <div className="slide">
                              <h3>Slide 2.3</h3>
                          </div>
                      </div>
                      <div className="section">
                          <h3>Section 3</h3>


                      </div>

                  </div>

              )}}
        />
        </div>
    );
}

function ThirtySection () {
    return (
        <div className="section" id="2">
            <div className="slide " data-anchor="slide1">
                <div className="historyWrapp y30">
                    <div className="gui__container h30">
                        <span className="title">Довоенные годы.</span>
                    </div>
                </div>
            </div>
            <div className="slide" data-anchor="slide2">
                <div className="gui__container slide-el">
                    <div className="banner">
                        <img src="img/other/15158332542f7.jpg" alt="" />
                    </div>
                    <div className="text">
                        <p>
                            До 1935 года в поселке «Текстильщики школы не было, детям
                            приходилось ездить в Москву или в Люблино. Лишь в феврале 1935
                            года в небольшом двухэтажном здании открылась начальная школа.
                            В тесном помещении с убогим оборудованием дети проучились
                            недолго: в 1936 году в Текстильщиках открылась школа –
                            новостройка № 475, а прежнее помещение было передано под
                            общежитие учителей. Поселок быстро рос, классы были
                            переполнены, и на этой же улице (475 школа изначально
                            располагалась в начале 1 улицы Текстильщиков, сейчас там
                            вспомогательная школа) началось строительство новой школы.
                        </p>
                    </div>
                </div>
            </div>
            <div className="slide" data-anchor="slide2">
                <div className="gui__container slide-el">
                    <div className="banner"></div>
                    <div className="text">
                        <p>
                            В 1939 году в рабочем посёлке была открыта семилетняя школа №
                            654, директором которой стал Иван Еремеевич Дерилло, а завучем
                            – Анна Герасимовна Рейснер. Наплыв учащихся в школу был очень
                            большой, да и территория микрорайона была огромной: дети,
                            живущие в железнодорожных бараках, в деревне Грайвороново, в
                            совхозе имени Горького (сегодня там стадион «Москвич»), дети
                            строителей пришли в 654 школу.
                        </p>
                    </div>
                </div>
            </div>
            <div className="slide" data-anchor="slide3">
                <div className="gui__container slide-el">
                    <div className="banner"></div>
                    <div className="text">
                        <p>
                            Самыми старшими в школе были два седьмых класса и в них один
                            комсомолец – шестнадцатилетний Роберт Мкртчан. Желание ребят
                            вступить в комсомол было огромным, и вместе со старшей
                            пионервожатой А.Пантюшиной они к этому готовились. Вскоре была
                            создана комсомольская организация школы, насчитывавшая сначала
                            около 20 человек. К концу учебного года в комсомол вступили
                            почти все семиклассники. В работе комсомольскому активу всегда
                            помогал директор. В школе проводились интересные тематические
                            вечера, литературные диспуты, были организованы технические и
                            художественные кружки, под руководством учителя физики
                            М.А.Прохоровника радиофицировали школу. Ребята выступали с
                            концертами перед рабочими совхоза, устраивали воскресники по
                            прополке и уборке овощей. Регулярно под руководством
                            бессменного редактора Коли Родина выходила стенгазета,
                            отражающая школьную жизнь.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default History;
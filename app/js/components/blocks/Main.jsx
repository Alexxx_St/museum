import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";

import History from "./History";
import Hall from "./Hall";
import Home from "./Home";
import Gallery from "./Gallery";


function Main() {

    return (
      <BrowserRouter>
        <div className="gui__container">
            <Route path="/" exact component={Home} />
            <Route path="/history" component={History} />
            <Route path="/hall" component={Hall} />
            <Route path="/gallery" component={Gallery} />
        </div>
      </BrowserRouter>
    );

}

export default Main;

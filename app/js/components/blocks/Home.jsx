import React, { Component } from "react";
import Navigation from "../elems/Navigation";

function Home () {
    return (
        <div className="gui__container home">
            <div className="gui__content">
                <div className="gui__content-welcome">
                    <span className="welcome-text">Добро пожаловать в музей поколений <span className="egg" > ШПЧ</span></span>
                    <div className="gui__nav"><Navigation/></div>
                </div>
            </div>
        </div>
    )
}


export default Home;

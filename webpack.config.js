var webpack = require('webpack');


  module.exports = {
    entry: './app/js/index.jsx',
    output: {
      path: __dirname + '/public',
      filename: 'bundle.js'
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            cacheDirectory: true,
            presets: ['react', 'es2015']
          }
        }
      ]
    }
  }


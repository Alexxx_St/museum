import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
//import Hall from "../blocks/Hall";

function Navigation() {
    return (
        <div className="nav_contaier">
          <ul>
            <li>
              <Link to="/gallery" >
                Галерея Учителей
              </Link>
            </li>
            <li>
              <Link to="/history" >
                История Школы
              </Link>
            </li>
            <li>
              <Link to="/hall">
                Залы Поколений
              </Link>
            </li>
          </ul>
        </div>
        );
}

export default Navigation;

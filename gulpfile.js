"use strict";

var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    less         = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    concatCss    = require('gulp-concat-css'),
    cleanCSS     = require('gulp-clean-css'),
    rename       = require("gulp-rename"),
    uglify       = require('gulp-uglify');


gulp.task('serve', ['less'], function() {
    gulp.watch("app/styles/css/*.css").on('change', browserSync.reload);
    gulp.watch("app/*.php").on('change', browserSync.reload);

});
gulp.task('less', function() {

    return gulp.src("app/styles/less/*.less")

        .pipe(less())

        .pipe(autoprefixer({

            browsers: ['last 2 versions'],

            cascade: false

        }))

        .pipe(concatCss("main.css"))

        .pipe(gulp.dest("public/css"))

        .pipe(browserSync.stream());

});

gulp.task('mincss', function() {

    return gulp.src("public/css/*.css")

        .pipe(rename({suffix: ".min"}))

        .pipe(cleanCSS())

        .pipe(gulp.dest("public/styles/css"));

});

gulp.task('minjs', function() {

    return gulp.src("app/js/*.js")

        .pipe(rename({suffix: ".min"}))

        .pipe(uglify())

        .pipe(gulp.dest("public/js"));

})
gulp.task('default', ['less', 'serve']);